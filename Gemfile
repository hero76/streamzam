source 'https://rubygems.org'

gem 'rails', '~> 5.2.1'
gem 'rake', '12.3.1'
gem 'rspec-rails'
gem 'mysql2'

# test local production
gem 'capistrano', '~> 3.7', '>= 3.7.1'
gem 'capistrano-rails', '~> 1.2'
gem 'capistrano-passenger', '~> 0.2.0'
gem 'capistrano-rvm'
gem "capistrano-yarn"
gem 'capistrano-bundler', '~> 1.4'


gem 'thredded'

# errors about timezone
gem 'tzinfo-data'

# greenfield
gem 'greenfield', path: 'greenfield'

# ruby
gem 'sometimes'
gem 'awesome_print', :require => 'ap'

# uploading
gem 'aws-sdk-s3'
gem 'paperclip'
gem 'rubyzip'
gem 'ruby-mp3info', :require => 'mp3info'
gem 'mime-types'
gem 'ruby-audio', require: false
gem 's3_direct_upload'

# active record
gem 'acts_as_list'
gem 'has_permalink'
gem 'authlogic', '~> 4.2'
gem 'scrypt' # for authlogic
gem 'request_store' # for authlogic
gem 'recaptcha', :require => 'recaptcha/rails'

# view
gem 'redcarpet'
gem 'country_select'
gem 'will_paginate'
gem 'dynamic_form'
gem 'simple_form'
gem 'local_time'
gem 'gemoji'

# deprecated 
gem 'record_tag_helper'

# external services
gem 'rakismet'
gem 'geokit'
gem 'postmark-rails'

# greenfield
gem 'font-awesome-sass'
gem 'jquery-fileupload-rails'

# frontend
gem 'webpacker', '>= 4.0.x'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'sass-rails'
gem 'compass-rails'
gem 'yui-compressor'
gem 'uglifier'
gem 'coffee-rails'
gem 'soundmanager2-rails'
gem 'turbolinks'
gem 'cloudfront-signer'

# monitoring & perf
gem 'sidekiq'
gem 'dalli'

group :production do
  gem 'puma'
end

group :development do
  gem 'meta_request'
  gem 'thin'
  gem 'sqlite3'
  gem 'perf_check'
  gem 'annotate'
  gem 'rubocop', '~> 0.60.0', require: false
end

## Who loves tests! You do? You do!
group :test do
  gem 'rspec-rails', :require => false
  gem 'rspec-mocks', :require => false
  gem 'guard-rspec', :require => false
  gem 'database_cleaner', :require => false
  gem 'rb-fsevent', :require => false
  gem 'guard', :require => false
  gem 'listen', :require => false
  # https://github.com/thoughtbot/factory_bot/wiki/Usage
  gem 'factory_bot_rails',:require => false
  gem 'rails-controller-testing'
end

group :development, :test do
  gem 'pry'
end
